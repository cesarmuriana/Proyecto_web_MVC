Admin:
	user: admin
	passw: admin

Normal user:
	user: cesar
	passw: cesar

--------------------------------------------

Hola me llamo cesar y este es mi primer proyecto MVC

Esta es una pagina de compra de criptomonedas. (Los precios estan recogidos de diferentes apis en tiempo real.)


--------------------------------------------

HOME/INICIO:

La pagina HOME tiene un carrusel y debajo un datatable con sitios donde poder comprar con bitcoins, estos sitios estan recogidos mediante una api.


--------------------------------------------

START/EMPEZAR:

En la pagina EMPEZAR hay un datatable donde recojemos un poco de informacion de los usuario desde la base de datos, abajo de esta tabla hay un select donde recoje las opciones desde la base de datos donde podemos elegir que tipo de cripomoneda queremos y esta restringe la busqueda en la barra de busqueda de al lado y a la misma vez solo deja visibles las criptomonedas de primero/segunda/tercera generacion o todas.
Tenemos un boton que dice "agregar al carrito" en todas las criptomonedas, estas se agregan al carrito y nos muestra el carrito con la criptomoneda.


--------------------------------------------

CARRITO:

El carrito muestra la cantidad (por defecto 1, y no puede haber 0 ni por debajo), tambien el preico unitario y el precio de la cantidad * el precio unitario, se puede eliminar y abajo sale el precio total en caso de que tengamos varias criptomonedas en el carrito.

(El precio se recoje mediante apis con promise primeramente recogiendo el precio en USD y luego recogiendo el cambio de USD a EUR y el precio en USD es calculado en EUR)

Si estamos logeados recojera el nombre de usuario y guardara en la base de datos la compra en forma de usuario,cantidad y producto, y si no estas logeado serà redireccionado al login y despues de hacer un login correcto serà redireccionado al carrito para realizar la compra.


--------------------------------------------

USER/USURAIO:

(Pagina user solo visible para admin)
En el apartado de user, tenemos el CRUD, en este podremos leer toda la informacion del usuario mediante el boton read esto se muestra mediante un modal, podremos actualizar usuarios mediante el boton update, podremos eliminar mediante el boton delete, podremos agregar un usuario mediante el boton + de arriba de la tabla de usuarios (no podremos añadir ningun usuario que ya existe ni poner un e-mail ni DNI que ya esté en la base de datos) y al lado de este hay un boton llamado DUMIES que este agrega 3 usuarios automaticamente, si alguno de estos ya existe no serà agregado.


--------------------------------------------

SERVICES/SERVICIOS:

En la pagina services tenemos lorem ipsum.


--------------------------------------------

CONOCENOS/ABAUT US:

En la pagina abaut us tenemos lorem ipsum y un mapa donde mostramos un supuesto centro de minado de criptomonedas.


--------------------------------------------

LENGUAGE:

El apartado lenguage del menu tenems un desplegable que podemos elegir un idioma español/ingles, este està realizado mediante php y no abarca toda la pagina por el echo de que fue una de las primeras cosas que se hicieron y por no atrasar el proyecto no se siguio haciendo (se puede ver en el menu y en USER)


--------------------------------------------

MENUS:

Tenemos 3 tipos de menus, uno para el admin (es el unico que puede ver la pagina de USER) y este no puede ver el carrito, otro para un usuario normal (si puede ver e l carrito pero no la pagina USER) y otro para un guest o usuario no registrado, el menu de admin y usuairo nomral tienen la diferencia que pone el nombre de usuario al lado de lenguage (esto no lo tiene el guest)


--------------------------------------------

LOGIN/LOG OUT:

En el login solo podremos entrar con un usuario registrado en la base de datos, si no esta lo notificara.

Hay un apartado de RECOVER PASSWRD donde se pedirà el e-mail y la contraseña nueva y repetir esta. (si el e-mail no existe serà notificado como error)

Y por ultimo hay un apartado de REGISTER donde se pedira un poco de informacion para registar al usuario.