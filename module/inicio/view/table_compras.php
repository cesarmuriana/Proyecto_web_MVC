<head>
    <link rel="stylesheet" href="module/user/model/table.css">
    <script src="module/inicio/model/library_datatable.js"></script>
    <script src="module/inicio/model/datatable.js"></script>

</head>
<table id="tablelist" class="display" cellspacing="0" width="100%">
			<thead>
                    <tr>
                        <td width=125><b><?php echo $lang_select['list_user']?></b></th>
                        <td width=125><b><?php echo $lang_select['list_name']?></b></th>
                        <td width=125><b><?php echo 'Preferencias'?></b></th>
                        <td width=125><b><?php echo 'Bitcoin'?></b></th>
                        <td width=125><b><?php echo 'IOTA'?></b></th>
                        <td width=125><b><?php echo 'Litecoin'?></b></th>
                        <td width=125><b><?php echo 'Ethereum'?></b></th>
                    </tr>
            </thead>
            <tfoot>
                <tr>
                    <td width=125><b><?php echo $lang_select['list_user']?></b></th>
                    <td width=125><b><?php echo $lang_select['list_name']?></b></th>
                    <td width=125><b><?php echo 'Preferencias'?></b></th>
                    <td width=125><b><?php echo 'Bitcoin'?></b></th>
                    <td width=125><b><?php echo 'IOTA'?></b></th>
                    <td width=125><b><?php echo 'Litecoin'?></b></th>
                    <td width=125><b><?php echo 'Ethereum'?></b></th>
                </tr>
            </tfoot>
            <tbody>
                    <?php
	            	if ($rdo->num_rows === 0){
                        echo '<tr>';
                        echo '<td align="center"  colspan="3">NINGUN USUARIO HA ECHO UNA COMPRA</td>';
                        echo '</tr>';
                    }else{
                        foreach ($rdo as $row) {
                            echo '<tr>';
                            echo '<td width=120>'. $row['usuario'] . '</td>';
                            //echo '<td width=100>'. $row['DNI'] . '</td>';
                            echo '<td width=100>'. $row['nombre'] . '</td>';
                                    ?><td><?php
                                    if ($row['aficiones']=="si"){
                                        if ($row['Bitcoin']=="si"){
                                        ?>
                                            Bitcoin
                                        <?php
                                        }
                                        if ($row['IOTA']=="si"){
                                        ?>
                                            IOTA
                                        <?php
                                        }
                                        if ($row['Litecoin']=="si") {
                                        ?>
                                            Litecoin
                                        <?php
                                        }
                                        if ($row['Ethereum']=="si"){
                                            ?>
                                            Ethereum
                                    <?php
                                        }
                                    }else{
                                        echo 'El usuario no tiene ninguna preferencia';
                                        }
                                    ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (!empty($row['bitcoin_compra'])){
                                            echo $row['bitcoin_compra'];
                                        }else{
                                            echo '0';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        if (!empty($row['iota_compra'])){
                                            echo $row['iota_compra'];
                                        }else{
                                            echo '0';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        
                                        if (!empty($row['litecoin_compra'])) {
                                            echo $row['litecoin_compra'];
                                        }else{
                                            echo '0';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        
                                        if (!empty($row['ethereum_compra'])){
                                            echo $row['ethereum_compra'];
                                        }else{
                                            echo '0';
                                        }
                                    ?>
                                    </td>
                                    <?php
                        }
                    }
                    echo '</tr>'   ?>
	        </tbody>
	</table>