<head>
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
    <script src="module/login/model/validate_user.js"></script>
</head>
<form autocomplete="on" method="post" name="recover" id="recover"">
 	<div id="login">
 		<h2 id="texto">Recuperar password</h2>
 		<img src="view/img/login.png" id="login_png">
    	<br>
    	<div id="input_login">
        	<table id="table_login">
        			<font color="red">
			        	<?php
			        		if(isset($error))
			        			echo $error
			        	?>
					</font>
				<div id="email_recover">
		            <tr>
		                <td>Introduzca su direccion de correo: </td>
		                <td><input type="text" id="email_reset" name="email_reset" placeholder="Email" value="<?php if(isset($_POST['email_reset']))echo $_POST['email_reset'] ?>" /></td>
		                <td><font color="red">
		                    <span id="e_email" class="e">
		                        <?php
		                        if (isset($error['email']))
                                	print_r($error['email']);

		                        ?>

		                    </span>
		                </font></font></td>
		            </tr>
	        	</div>
	        	<div id="pass_recover">
		            <tr>
		                <td>Contraseña: </td>
		                <td><input type="password" id="pass_reset" name="pass_reset" placeholder="contraseña" value=""/></td>
		                <td><font color="red">
		                    <span id="e_password" class="e">
		                        <?php
		                            if (isset($error['password']))
		                                print_r($error['password']);
		                        ?>
		                    </span>
		                </font></font></td>
		            </tr>
		            <tr>
		                <td>Contraseña: </td>
		                <td><input type="password" id="pass_reset2" name="pass_reset2" placeholder="contraseña" value=""/></td>
		                <td><font color="red">
		                    <span id="e_password2" class="e">
		                        <?php
		                            if (isset($error['password']))
		                                print_r($error['password']);
		                        ?>
		                    </span>
		                </font></font></td>
		            </tr>
		        </div>
	        </table>
        </div>
        <table id="botones_login">
	        <td><input type="hidden" value="recover" name="recover" id="recover"/></td>
	        <td><input type="button" value="Reset" id="reset_btn" onclick="validate_reset()"/></td>
	        <td align="right"><a id="volver" href="index.php?page=controller_login&op=login">Volver</a></td>
    	</table>
	</div>
</form>