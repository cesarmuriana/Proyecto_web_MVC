<head>
	<script type="text/javascript" src="http://code.jquery.com/jquery-2.2.4.min.js"></script>
	<script type="text/javascript" src="module/login/model/validate_user.js"></script>
</head>
<form autocomplete="on" method="post" name="login" id="login"">
 	<div id="login">
 		<h2 id="texto">Login</h2>
 		<img src="view/img/login.png" id="login_png">
    	<br>
    	<div id="input_login">
        	<table id="table_login">
        			<font color="red">
			        	<?php
			        		if(isset($error))
			        			echo $error
			        	?>
					</font>
	            <tr>
	                <td>Usuario: </td>
	                <td><input type="text" id="usuario_login" name="usuario_login" placeholder="usuario" value="<?php if(isset($_POST['usuario_login']))echo $_POST['usuario_login'] ?>" /></td>
	                <td><font color="red">
	                    <span id="e_usuario" class="e">
	                        <?php
	                        if (isset($error['usuario']))
	                            print_r($error['usuario']);
	                        ?>
	                    </span>
	                </font></font></td>
	            </tr>
	            <tr>
	                <td>Contraseña: </td>
	                <td><input type="password" id="pass_login" name="pass_login" placeholder="contraseña" value=""/></td>
	                <td><font color="red">
	                    <span id="e_password" class="e">
	                        <?php
	                            if (isset($error['password']))
	                                print_r($error['password']);
	                        ?>
	                    </span>
	                </font></font></td>
	            </tr>
	        </table>
        </div>
        <table id="botones_login">
	        <td><input type="hidden" value="login" name="login" id="login"/></td>
	        <td><input type="button" value="Login" id="login_btn" onclick="validate_user_login()"/></td>
        	<td><a type="button" value="Reset" id="reset_btn" href="index.php?page=controller_login&op=reset"/>He olvidado mi contraseña</a></td>
	        <td><a type="button" value="Register" id="register_btn" href="index.php?page=controller_login&op=register"/>Register</a></td>
    	</table>
	</div>
</form>