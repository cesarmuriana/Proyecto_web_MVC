<head>
    <script src="module/login/model/validate_user.js"></script>
</head>
<div>
    <form autocomplete="on" method="post" name="register_user" id="register_user">
        <h1 id="texto">Register</h1>
        <img src="view/img/register.png" id="registerpng">
        <br>
        <div id="contenido_register">
            <table id="register">
                    <tr>
                        <td>Usuario: </td>
                        <td><input type="text" id="usuario" name="usuario" placeholder="usuario" value="<?php if(isset($_POST['usuario']))echo $_POST['usuario'] ?>"/></td>
                        <td><font color="red">
                            <span id="e_usuario" class="e">
                                <?php
                                if (isset($error['usuario']))
                                    print_r($error['usuario']);
                         //       onsubmit="return validate();" action="index.php?page=controller_user&op=create"
                                if (isset($existe_user))
                                    print_r("Este nombre de usuario no esta disponible");
                                ?>
                            </span>
                        </font></font></td>
                    </tr>

                    <tr>
                        <td>Nombre: </td>
                        <td><input type="text" id="nombre" name="nombre" placeholder="nombre" value="<?php if(isset($_POST['nombre']))echo $_POST['nombre'] ?>"/></td>
                        <td><font color="red">
                            <span id="e_nombre" class="e">
                                <?php
                                  if (isset($error['nombre']))
                                    print_r($error['nombre']);
                                ?>
                            </span>
                        </font></font></td>
                    </tr>

                    <tr>
                        <td>Email: </td>
                        <td><input type="text" id="email" name="email" placeholder="email" value="<?php if(isset($_POST['email']))echo $_POST['email'] ?>"/></td>
                        <td><font color="red">
                            <span id="e_email" class="e">
                                <?php
                                    if (isset($error['email']))
                                        print_r($error['email']);
                                    if (isset($existe_email))
                                        print_r("Este email ya ha sido asociado a otro usuario");
                         //           echo "$e_email";
                                ?>
                            </span>
                        </font></font></td>
                    </tr>

                    <tr>
                        <td>Contraseña: </td>
                        <td><input type="password" id="pass" name="pass" placeholder="contraseña" value=""/></td>
                        <td><font color="red">
                            <span id="e_password" class="e">
                                <?php
                                    if (isset($error['pass']))
                                        print_r($error['pass']);
                                ?>
                            </span>
                        </font></font></td>
                    </tr>
            </table>
        </div>
        <table id="botones_register">
            <tr>
                <input type="hidden" value="register" name="register" id="register"/>
                <td><input type="button" value="Crear" name="create" id="aceptar" onclick="validate_register()"/></td>
                <td align="right"><a id="volver" href="index.php?page=controller_login&op=login">Volver</a></td>
            </tr>
        </table>
    </form>
</div>