<?php
$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/8_MVC_CRUD/';
include ($path."module/login/model/DAOLogin.php");
   

	if (!isset($_GET['op']))
	    	$_GET['op']='login';


   switch($_GET['op']){
    	case 'login';
            if (isset($_POST['login'])){
                $daologin = new DAOLogin();
                try{
                    $rdo = $daologin->find_user($_POST['usuario_login']);
                    if(isset($rdo)){
                        $usuario=get_object_vars($rdo);
                        if (isset($usuario)){
                            if (($usuario['pass'])==($_POST['pass_login'])){
                                $_SESSION['type']=$usuario['type'];
                                $_SESSION['user']=$usuario['usuario'];
                                $callback = 'index.php';
                                die('<script>window.location.href="'.$callback .'";</script>');
                            }else{
                                $error="La contraseña es incorrecta";
                            };
                        };
                    }else{
                        $error="El usuario no existe";
                    };

                }catch (Exception $e){
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                };
                
            };
            include("module/login/view/login.php");
    		break;
        case 'logout';
                $_SESSION['type']='';
                $_SESSION['user']='';
                session_unset();
                $callback = 'index.php?page=controller_login&op=login';
                die('<script>window.location.href="'.$callback .'";</script>');
            break;
		case 'register';
            include("module/login/model/validate.php");
           
            $check = true;
            
            if (isset($_POST['register'])){
                $check=validate_user();
                $error=$check['error'];
                if (empty($error)){
                    $_SESSION['usuario']=$_POST;
                    try{
                        $daologin = new DAOLogin($_POST['usuario']);
                        $existe_user = $daologin->find_user($_POST['usuario']);
                        $existe_email = $daologin->find_email($_POST['email']);
                        if ((!isset($existe_user)) and (!isset($existe_email))){
    		                $rdo = $daologin->insert_user_register($_POST);
                            echo '<script language="javascript">alert("Usuario registrado con exito")</script>';
                            $callback = 'index.php?page=controller_login&op=login';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
        			    die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    
                    if(isset($existe_user)){
                        echo '<script language="javascript">alert("Este usuario ya ha sido asociado")</script>';
                    }elseif(isset($existe_dni)){
                        echo '<script language="javascript">alert("Este DNI esta asociado a otro usuario")</script>';
                    }elseif(isset($existe_email)){
                        echo '<script language="javascript">alert("Este email esta asociado a otro usuario")</script>';
                        }else{
        		            if($rdo){
                       			echo '<script language="javascript">alert("Registrado en la base de datos correctamente")</script>';
                    			$callback = 'index.php?page=controller_user&op=list';
                			    die('<script>window.location.href="'.$callback .'";</script>');
                    		}else{
                    			$callback = 'index.php?page=503';
            			        die('<script>window.location.href="'.$callback .'";</script>');
                    		};
                        };
                };
            };
            include("module/login/view/register_user.php");
		    break;
        case 'reset';
                if (isset($_POST['recover'])){
                    $daologin = new DAOLogin();
                    $existe_email = $daologin->find_email($_POST['email']);
                    if (!isset($existe_email)){
                        $error['email']='El email no existe';
                        echo json_encode($error);
                        die();
                    }else{
                        try{
                            $reset_pass = $daologin->recover_user($_POST);
                        }catch (Exception $e){
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                        if($reset_pass="true"){
                            echo '<script language="javascript">alert("Cambio de clave realizado con exito")</script>';
                            $callback = 'index.php?page=controller_user&op=list';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }else{
                            $callback = 'index.php?page=503';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                    }
                }
                include($path."module/login/view/reset.php");
            break;
        case 'compra';
            if (isset($_POST['login'])){
                $daologin = new DAOLogin();
                try{
                    $rdo = $daologin->find_user($_POST['usuario_login']);
                    if(isset($rdo)){
                        $usuario=get_object_vars($rdo);
                        if (isset($usuario)){
                            if (($usuario['pass'])==($_POST['pass_login'])){
                                $_SESSION['type']=$usuario['type'];
                                $_SESSION['user']=$usuario['usuario'];
                                $callback = 'index.php?page=controller_carrito&op=carrito';
                                die('<script>window.location.href="'.$callback .'";</script>');
                            }else{
                                $error="La contraseña es incorrecta";
                            };
                        };
                    }else{
                        $error="El usuario no existe";
                    };

                }catch (Exception $e){
                    $callback = 'index.php?page=503';
                    die('<script>window.location.href="'.$callback .'";</script>');
                };
                
            };
            include("module/login/view/login.php");
            break;
    }