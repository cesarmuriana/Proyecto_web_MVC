<?php
        function validate_user(){
        $error='';
        $filtro = array(

            'usuario' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^[a-zA-Z0-9]*$/')
            ),
            
            'nombre' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^[a-zA-Z0-9]*$/')
            ),

            'apellidos' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^[a-zA-Z0-9]*$/')
            ),

            'email' => array(
                'filter'=>FILTER_CALLBACK,
                'options'=>'validatemail'
            ),
            
            'DNI' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^[0-9]{8}[A-Z]$/')
            ),
            
            'domicilio' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^\D{3,30}$/')
            ),
            'wallet' => array(
                'filter'=>FILTER_VALIDATE_REGEXP,
                'options'=>array('regexp'=>'/^[a-zA-Z0-9]*$/')
            )
            );
        
        $resultado=filter_input_array(INPUT_POST,$filtro);
        if(!$resultado['usuario']){
            $error['usuario']='Usuario debe tener de 4 a 20 caracteres';
        }elseif(!$resultado['nombre']){
            $error['nombre']='Nombre debe tener de 3 a 30 caracteres';
        }elseif(!$resultado['apellidos']){
            $error['apellidos']='Apellidos debe tener de 4 a 120 caracteres';
        }elseif(!$resultado['email']){
            $error['email']='El email debe contener de 5 a 50 caracteres y debe ser un email valido';
        }elseif(($_POST['pass'])!=($_POST['pass2'])){
            $error['password']='Las dos contrasenyas deben ser iguales';
        }elseif(!$resultado['domicilio']){
            $error['domicilio']='Domicilio debe tener de 3 a 30 caracteres';
        }elseif(!$resultado['wallet']){
            $error['wallet']='Tiene que ser una wallet valida';
        }elseif(!$resultado['DNI']){
            $error['DNI']='Tiene que ser un DNI valido';
        }else{
             return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
        };
        return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
    };

    function validatemail($email){
            $email = filter_var($email, FILTER_SANITIZE_EMAIL);
            if(filter_var($email, FILTER_VALIDATE_EMAIL)){
                if(filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp'=> '/^.{5,50}$/')))){
                    return $email;
                }
            }
            return false;
    }
    
    function debug($array){
        echo "<pre>";
        print_r($array);
        echo "</pre><br>";
    }
?>
