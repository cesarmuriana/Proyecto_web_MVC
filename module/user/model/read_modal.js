$(document).ready(function () {
        $('.Button_blue').click(function () {
            var id = this.getAttribute('id');
            console.log(id);
            $.ajax({
            type: "POST",
            url: "module/user/controller/controller_user.php?op=read&id=" + id,
            success: function(data){
            console.log(data);
                var json = JSON.parse(data);
                console.log(json);
                
                if(json === 'error') {
                    //pintar 503
                    window.location.href='index.php?page=503';
                }else{
                    console.log(json.usuario);
                    $("#user1").html(json.usuario);
                    $("#name").html(json.nombre);
                    $("#ape").html(json.apellidos);
                    $("#email").html(json.email);
                    $("#pass").html(json.pass);
                    $("#fnac").html(json.date_bithday);
                    $("#domi").html(json.domicilio);
                    $("#wallet").html(json.wallet);
                    $("#reg").html(json.fecha);
                    $("#DNI").html(json.DNI);
                    $("#sex").html(json.sexo);
                    $("#details_user").show();
                    $("#user_modal").dialog({
                        width: 850, //<!-- ------------- ancho de la ventana -->
                        height: 500, //<!--  ------------- altura de la ventana -->
                        //show: "scale", //<!-- ----------- animación de la ventana al aparecer -->
                        //hide: "scale", //<!-- ----------- animación al cerrar la ventana -->
                        resizable: "false", //<!-- ------ fija o redimensionable si ponemos este valor a "true" -->
                        //position: "down",<!--  ------ posicion de la ventana en la pantalla (left, top, right...) -->
                        modal: "true", //<!-- ------------ si esta en true bloquea el contenido de la web mientras la ventana esta activa (muy elegante) -->
                        buttons: {
                            Ok: function () {
                                $(this).dialog("close");
                            }
                        },
                        show: {
                            effect: "blind",
                            duration: 1000
                        },
                        hide: {
                            effect: "explode",
                            duration: 1000
                        }
                    });
                }//end-else
            }
            });
        });
});