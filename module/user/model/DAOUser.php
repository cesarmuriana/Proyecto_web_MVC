<?php
    include($path."model/connect.php");
    
	class DAOUser{
		function insert_user($datos){
			$user=$datos['usuario'];
        	$nom=$datos['nombre'];
        	$apell=$datos['apellidos'];
        	$email=$datos['email'];
        	$passwrd=$datos['pass'];
            $date = explode('/', $datos['fecha_nacimiento']);
        	$birthdate="$date[2]-$date[1]-$date[0]";
        	$domi=$datos['domicilio'];
        	$wallet=$datos['wallet'];
      //      $hora=$datos['hora'];
      //      $fecha=$datos['fecha'];
            $dni=$datos['DNI'];
            $sexo=$datos['sexo'];
        	
            if (!empty($datos['Bitcoin'])){
                $Bitcoin=$datos['Bitcoin'];
            }else{
                $Bitcoin="No";
            }

            if (!empty($datos['IOTA'])){
                $IOTA=$datos['IOTA'];
            }else{
                $IOTA="No";
            }

            if (!empty($datos['Litecoin'])){
                $Litecoin=$datos['Litecoin'];
            }else{
                $Litecoin="No";
            }

            if (!empty($datos['Ethereum'])){
                $Ethereum=$datos['Ethereum'];
            }else{
                $Ethereum="No";
            }

            if ((empty($datos['Bitcoin']))&&(empty($datos['IOTA']))&&(empty($datos['Litecoin']))&&(empty($datos['Ethereum']))){
                $aficiones="No";
            }else{
                $aficiones="Si";
            }

            $type='user';
        	$sql = "INSERT INTO `dbitcoin`.`usuario` (`usuario`, `nombre`, `apellidos`, `email`, `pass`, `date_bithday`, `domicilio`, `wallet`, `hora`, `fecha`, `DNI`, `sexo`, `aficiones`, `Bitcoin`, `IOTA`, `Litecoin`, `Ethereum`,`type`) VALUES ('$user', '$nom', '$apell', '$email', '$passwrd', '$birthdate', '$domi', '$wallet', now(), now(), '$dni', '$sexo', '$aficiones', '$Bitcoin', '$IOTA', '$Litecoin', '$Ethereum','$type')";

            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
			return $res;
		}
		
		function select_all_user(){
			$sql = "SELECT * FROM usuario ORDER BY usuario ASC";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
		}
		
		function select_user($user){
			$sql = "SELECT * FROM usuario WHERE usuario='$user'";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
           // print_r($res);
           // die();
            return $res;
		}
		
		function update_user($datos){
			$user=$datos['usuario'];
            $nom=$datos['nombre'];
            $apell=$datos['apellidos'];
            $email=$datos['email'];
            $passwrd=$datos['pass'];
            $birthdate=$datos['fecha_nacimiento'];
            $domi=$datos['domicilio'];
            $wallet=$datos['wallet'];
      //      $hora=$datos['hora'];
      //      $fecha=$datos['fecha'];
            $sexo=$datos['sexo'];
            
            if (!empty($datos['Bitcoin'])){
                $Bitcoin=$datos['Bitcoin'];
            }else{
                $Bitcoin="No";
            }

            if (!empty($datos['IOTA'])){
                $IOTA=$datos['IOTA'];
            }else{
                $IOTA="No";
            }

            if (!empty($datos['Litecoin'])){
                $Litecoin=$datos['Litecoin'];
            }else{
                $Litecoin="No";
            }

            if (!empty($datos['Ethereum'])){
                $Ethereum=$datos['Ethereum'];
            }else{
                $Ethereum="No";
            }

            if ((empty($datos['Bitcoin']))&&(empty($datos['IOTA']))&&(empty($datos['Litecoin']))&&(empty($datos['Ethereum']))){
                $aficiones="No";
            }else{
                $aficiones="Si";
            }

            $sql = "UPDATE `dbitcoin`.`usuario` SET `nombre`='$nom', `apellidos`='$apell', `email`='$email', `pass`='$passwrd', `date_bithday`='$birthdate', `domicilio`='$domi', `wallet`='$wallet', `sexo`='$sexo', `aficiones`='$aficiones',`Bitcoin`='$Bitcoin', `IOTA`='$IOTA', `Litecoin`='$Litecoin', `Ethereum`='$Ethereum' where `usuario`='$user'";

            
            //UPDATE cotxe
            //SET marca='SEAT',model='CORDOBA',color='BLANC',kms='56897' 
            //where matricula='3245CSX'
            
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);

            return $res;
		}
		
		function delete_user($user){
			$sql = "DELETE FROM usuario WHERE usuario='$user'";
			
			$conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
		}

        function dumies(){
                $primero="('User-123', 'angel', 'ancoca', 'angelcoca@gmail.com', '123456LALA', '1993-19-07', 'C/ Copérnico nº6 (Pol. Ind. De La Grela) Edificio BCA 28. Oficina: -1.1', '1HdfV7iKMRbpvKZu8QmJtuXBzFMn3UiHjy', now(), now(), '95646231k', 'Hombre', 'si', 'si', 'no', 'si', 'si')";

                $segundo="('User-124', 'david', 'daurgil', 'davidgil@gmail.com', '123456LOLO', '1990-06-06', 'Avenida Pintor Sorolla 125 4ºG', '3L1WohGdpS3ARz8W5ofVgZBCV94n9fQ2wj', now(), now(), '78949524a', 'Hombre', 'si', 'no', 'si', 'no', 'si')";

                $tercero="('User-125', 'usuarionom', 'usuarioapell', 'usuarionomapell@gmail.com', '123456SUP', '1980-05-16', 'Calle San Romualdo 13', '3GyqjsSqJ5bt1n7mRihm1kAGRsgJBoqp9A', now(), now(), '48601445l', 'Hombre', 'no', 'no', 'no', 'no', 'no')";
                
                $daouser = new DAOUser();
                $existe_user_123=$daouser->find_user("User-123");
                $existe_user_124=$daouser->find_user("User-124");
                $existe_user_125=$daouser->find_user("User-125");

            if((isset($existe_user_123))&(isset($existe_user_124))&(isset($existe_user_125))){
                $res='null';
                return $res;
            };
            if((!isset($existe_user_123))&(!isset($existe_user_124))&(!isset($existe_user_125))){
                $sql = "INSERT INTO `dbitcoin`.`usuario` (`usuario`, `nombre`, `apellidos`, `email`, `pass`, `date_bithday`, `domicilio`, `wallet`, `hora`, `fecha`, `DNI`, `sexo`, `aficiones`, `Bitcoin`, `IOTA`, `Litecoin`, `Ethereum`) VALUES $primero,$segundo,$tercero";
            };

            if((!isset($existe_user_123))&(!isset($existe_user_124))&(isset($existe_user_125))){
                $sql = "INSERT INTO `dbitcoin`.`usuario` (`usuario`, `nombre`, `apellidos`, `email`, `pass`, `date_bithday`, `domicilio`, `wallet`, `hora`, `fecha`, `DNI`, `sexo`, `aficiones`, `Bitcoin`, `IOTA`, `Litecoin`, `Ethereum`) VALUES $primero,$segundo";
            };
            if((isset($existe_user_123))&(isset($existe_user_124))&(!isset($existe_user_125))){
                $sql = "INSERT INTO `dbitcoin`.`usuario` (`usuario`, `nombre`, `apellidos`, `email`, `pass`, `date_bithday`, `domicilio`, `wallet`, `hora`, `fecha`, `DNI`, `sexo`, `aficiones`, `Bitcoin`, `IOTA`, `Litecoin`, `Ethereum`) VALUES $tercero";
            };

            if((!isset($existe_user_123))&(isset($existe_user_124))&(!isset($existe_user_125))){
                $sql = "INSERT INTO `dbitcoin`.`usuario` (`usuario`, `nombre`, `apellidos`, `email`, `pass`, `date_bithday`, `domicilio`, `wallet`, `hora`, `fecha`, `DNI`, `sexo`, `aficiones`, `Bitcoin`, `IOTA`, `Litecoin`, `Ethereum`) VALUES $primero,$tercero";
            };
            if((isset($existe_user_123))&(!isset($existe_user_124))&(isset($existe_user_125))){
                $sql = "INSERT INTO `dbitcoin`.`usuario` (`usuario`, `nombre`, `apellidos`, `email`, `pass`, `date_bithday`, `domicilio`, `wallet`, `hora`, `fecha`, `DNI`, `sexo`, `aficiones`, `Bitcoin`, `IOTA`, `Litecoin`, `Ethereum`) VALUES $segundo";
            };

            if((!isset($existe_user_123))&(isset($existe_user_124))&(isset($existe_user_125))){
                $sql = "INSERT INTO `dbitcoin`.`usuario` (`usuario`, `nombre`, `apellidos`, `email`, `pass`, `date_bithday`, `domicilio`, `wallet`, `hora`, `fecha`, `DNI`, `sexo`, `aficiones`, `Bitcoin`, `IOTA`, `Litecoin`, `Ethereum`) VALUES $primero";
            };
            if((isset($existe_user_123))&(!isset($existe_user_124))&(!isset($existe_user_125))){
                $sql = "INSERT INTO `dbitcoin`.`usuario` (`usuario`, `nombre`, `apellidos`, `email`, `pass`, `date_bithday`, `domicilio`, `wallet`, `hora`, `fecha`, `DNI`, `sexo`, `aficiones`, `Bitcoin`, `IOTA`, `Litecoin`, `Ethereum`) VALUES $segundo,$tercero";
            };

            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql);
            connect::close($conexion);
            return $res;
        }

        function find_user($user){
            $sql = "SELECT * FROM usuario WHERE usuario='$user'";
            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
        }

        function find_dni($DNI){
            $sql = "SELECT * FROM usuario WHERE DNI='$DNI'";

            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
        }
        function find_email($email){
            $sql = "SELECT * FROM usuario WHERE email='$email'";

            $conexion = connect::con();
            $res = mysqli_query($conexion, $sql)->fetch_object();
            connect::close($conexion);
            return $res;
        }
    }