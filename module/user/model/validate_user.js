function validate_user() {
    if (document.update_user.usuario.value.length===0){
        document.getElementById('e_usuario').innerHTML = "Tiene que escribir su usuario";
        document.update_user.usuario.focus();
        return 0;
    }
    document.getElementById('e_usuario').innerHTML = "";
    
    if (document.update_user.nombre.value.length===0){
        document.getElementById('e_nombre').innerHTML = "Tiene que escribir su nombre";
        document.update_user.nombre.focus();
        return 0;
    }
    document.getElementById('e_nombre').innerHTML = "";
    
    if (document.update_user.apellidos.value.length===0){
        document.getElementById('e_apellidos').innerHTML = "Tiene que escribir sus apellidos";
        document.update_user.apellidos.focus();
        return 0;
    }
    document.getElementById('e_apellidos').innerHTML = "";
    
    if (document.update_user.email.value.length===0){
        document.getElementById('e_email').innerHTML = "Tiene que escribir su email";
        document.update_user.email.focus();
        return 0;
    }
    document.getElementById('e_email').innerHTML = "";
    
    if (document.update_user.pass.value.length===0){
        document.getElementById('e_password').innerHTML = "Tiene que escribir su password";
        document.update_user.pass.focus();
        return 0;
    }
    document.getElementById('e_password').innerHTML = "";
    
    if (document.update_user.pass2.value.length===0){
        document.getElementById('e_password2').innerHTML = "Repite la contraseña";
        document.update_user.pass2.focus();
        return 0;
    }
    document.getElementById('e_password2').innerHTML = "";
   
    if (document.update_user.domicilio.value.length===0){
        document.getElementById('e_domicilio').innerHTML = "Tiene que escribir su domicilio";
        document.update_user.domicilio.focus();
        return 0;
    }
    document.getElementById('e_domicilio').innerHTML = "";

    if (document.update_user.wallet.value.length===0){
        document.getElementById('e_wallet').innerHTML = "Tiene que introducir su wallet";
        document.update_user.wallet.focus();
        return 0;
    }
    document.getElementById('e_wallet').innerHTML = "";

    if (document.update_user.DNI.value.length===0){
        document.getElementById('e_DNI').innerHTML = "Tiene que escribir su DNI";
        document.update_user.DNI.focus();
        return 0;
    }
    document.getElementById('e_DNI').innerHTML = "";

    if (document.update_user.sexo.value.length===0){
        document.getElementById('e_sexo').innerHTML = "Tiene que elegir uno de los dos sexos";
        document.update_user.sexo.focus();
        return 0;
    }
    document.getElementById('e_DNI').innerHTML = "";


    document.update_user.submit();
    document.update_user.action="index.php?page=controller_users";

}

function validate_user_create() {
    if (document.create_user.usuario.value.length===0){
        document.getElementById('e_usuario').innerHTML = "Tiene que escribir su usuario";
        document.create_user.usuario.focus();
        return 0;
    }
    document.getElementById('e_usuario').innerHTML = "";
    
    if (document.create_user.nombre.value.length===0){
        document.getElementById('e_nombre').innerHTML = "Tiene que escribir su nombre";
        document.create_user.nombre.focus();
        return 0;
    }
    document.getElementById('e_nombre').innerHTML = "";
    
    if (document.create_user.apellidos.value.length===0){
        document.getElementById('e_apellidos').innerHTML = "Tiene que escribir sus apellidos";
        document.create_user.apellidos.focus();
        return 0;
    }
    document.getElementById('e_apellidos').innerHTML = "";
    
    if (document.create_user.email.value.length===0){
        document.getElementById('e_email').innerHTML = "Tiene que escribir su email";
        document.create_user.email.focus();
        return 0;
    }
    document.getElementById('e_email').innerHTML = "";
    
    if (document.create_user.pass.value.length===0){
        document.getElementById('e_password').innerHTML = "Tiene que escribir su password";
        document.create_user.pass.focus();
        return 0;
    }
    document.getElementById('e_password').innerHTML = "";
    
    if (document.create_user.pass2.value.length===0){
        document.getElementById('e_password2').innerHTML = "Repite la contraseña";
        document.create_user.pass2.focus();
        return 0;
    }
    document.getElementById('e_password2').innerHTML = "";
   
    if (document.create_user.domicilio.value.length===0){
        document.getElementById('e_domicilio').innerHTML = "Tiene que escribir su domicilio";
        document.create_user.domicilio.focus();
        return 0;
    }
    document.getElementById('e_domicilio').innerHTML = "";

    if (document.create_user.wallet.value.length===0){
        document.getElementById('e_wallet').innerHTML = "Tiene que introducir su wallet";
        document.create_user.wallet.focus();
        return 0;
    }
    document.getElementById('e_wallet').innerHTML = "";

    if (document.create_user.DNI.value.length===0){
        document.getElementById('e_DNI').innerHTML = "Tiene que escribir su DNI";
        document.create_user.DNI.focus();
        return 0;
    }
    document.getElementById('e_DNI').innerHTML = "";

    if (document.create_user.sexo.value.length===0){
        document.getElementById('e_sexo').innerHTML = "Tiene que elegir uno de los dos sexos";
        document.create_user.sexo.focus();
        return 0;
    }
    document.getElementById('e_sexo').innerHTML = "";


    document.create_user.submit();
    document.create_user.action="index.php?page=controller_users";

}