<div id="contenido">
    <form autocomplete="on" method="post" name="create_user" id="create_user">
        <h1>Usuario nuevo</h1>
        <img src="view/img/register.png" id="anadirpng">
        <br>
        <div id="contenido_anadir">
            <table id="anadir_user">
                <tr>
                    <td>Usuario: </td>
                    <td><input type="text" id="usuario" name="usuario" placeholder="usuario" value="<?php if(isset($_POST['usuario']))echo $_POST['usuario'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_usuario" class="e">
                            <?php
                            if (isset($error['usuario']))
                                print_r($error['usuario']);

                            if (isset($existe_user))
                                print_r("Este nombre de usuario no esta disponible");
                            ?>
                        </span>
                    </font></font></td>
                </tr>
            
                <tr>
                    <td>Nombre: </td>
                    <td><input type="text" id="nombre" name="nombre" placeholder="nombre" value="<?php if(isset($_POST['nombre']))echo $_POST['nombre'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_nombre" class="e">
                            <?php
                              if (isset($error['nombre']))
                                print_r($error['nombre']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Apellidos: </td>
                    <td><input type="text" id="apellidos" name="apellidos" placeholder="apellidos" value="<?php if(isset($_POST['apellidos']))echo $_POST['apellidos'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_apellidos" class="e">
                            <?php
                                if (isset($error['apellidos']))
                                    print_r($error['apellidos']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Email: </td>
                    <td><input type="text" id="email" name="email" placeholder="email" value="<?php if(isset($_POST['email']))echo $_POST['email'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_email" class="e">
                            <?php
                                if (isset($error['email']))
                                    print_r($error['email']);
                                if (isset($existe_email))
                                    print_r("Este email ya ha sido asociado a otro usuario");
                     //           echo "$e_email";
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Contraseña: </td>
                    <td><input type="password" id="pass" name="pass" placeholder="contraseña" value=""/></td>
                    <td><font color="red">
                        <span id="e_password" class="e">
                            <?php
                                if (isset($error['password']))
                                    print_r($error['password']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Repite Contraseña: </td>
                    <td><input type="password" id="pass2" name="pass2" placeholder="Repite contraseña" value=""/></td>
                    <td><font color="red">
                        <span id="e_password2" class="e">
                            <?php
                                if (isset($error['password']))
                                    print_r($error['password']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>
                
                <tr>
                    <td>Fecha de nacimiento: </td>
                    <td><input id="fecha" type="text" name="fecha_nacimiento" placeholder="fecha de nacimiento" readonly value=""/></td>
                    <td><font color="red">
                        <span id="e_fecha_nacimiento" class="e">
                            <?php
                                if (isset($error['date_birthday']))
                                    print_r($error['date_birthday']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Domicilio: </td>
                    <td><input type="text" id="domicilio" name="domicilio" placeholder="domicilio" value="<?php if(isset($_POST['domicilio']))echo $_POST['domicilio'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_domicilio" class="e">
                            <?php
                                if (isset($error['domicilio']))
                                    print_r($error['domicilio']);                        
                            ?>
                       </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Wallet: </td>
                    <td><input type="text" id="wallet" name="wallet" placeholder="wallet" value="<?php if(isset($_POST['wallet']))echo $_POST['wallet'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_wallet" class="e">
                            <?php
                                if (isset($error['wallet']))
                                    print_r($error['wallet']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>DNI: </td>
                    <td><input type="text" id= "DNI" name="DNI" placeholder="DNI" value="<?php if(isset($_POST['DNI']))echo $_POST['DNI'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_DNI" class="e">
                            <?php
                                if (isset($error['DNI']))
                                    print_r($error['DNI']);
                                if (isset($existe_dni))
                                    print_r("Este DNI ya ha sido asociado a un usuario");
                            ?>
                        </span>
                    </font></font></td>
                </tr>
                
                <tr>
                    <td>Sexo: </td>
                    <td><input type="radio" id="sexo" name="sexo" placeholder="sexo" value="Hombre"/>Hombre
                        <input type="radio" id="sexo" name="sexo" placeholder="sexo" value="Mujer"/>Mujer</td>
                    <td><font color="red">
                        <span id="e_sexo" class="e">
                            <?php
                                if (isset($error['sexo']))
                                    print_r($error['sexo']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>
                    
                </tr>
                
                <tr>
                    <td>Interesado en: </td>
                    <td><input type="checkbox" id="Bitcoin" name="Bitcoin" value="Si"/>Bitcoin
                        <input type="checkbox" id="IOTA" name="IOTA" value="Si"/>IOTA
                        <input type="checkbox" id="Litecoin" name="Litecoin" value="Si"/>Litecoin
                        <input type="checkbox" id="Ethereum" name="Ethereum" value="Si"/>Ethereum</td>
                    <td><font color="red">
                        <span id="e_aficion" class="e">
                            <?php
                     //           print_r ($error_aficion);
                            ?>
                        </span>
                    </font></font></td>
                </tr>
                
                <tr>
                    <input type="hidden" value="create" name="create" id="create"/>
                    <td><input type="button" value="Crear" name="create" id="aceptar" onclick="validate_user_create()"/></td>
                    <td align="right"><a id="volver" href="index.php?page=controller_user&op=list">Volver</a></td>
                </tr>
            </table>
        </div>
    </form>
</div>