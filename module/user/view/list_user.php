<?php
if ($_SESSION['type']!='admin'){
    $callback = 'index.php?page=503';
    die('<script>window.location.href="'.$callback .'";</script>');
}
?>
<head>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.11.4/jquery-ui.js"></script>
    <script src="module/user/model/read_modal.js"></script>
</head>
<div id="contenido">
    <div class="container">
    	<div class="lista">
    		<h2 id="texto">LISTA DE USUARIOS</h2>
    	</div>
    	<div class="lista">
            <div id="menu_creacion">
        		<a href="index.php?page=controller_user&op=create"><img id="anadir" src="view/img/mas.png"></a>
                <?php
                echo '<a id="Dumies" class="Dumies" href="index.php?page=controller_user&op=dumies">Dumies</a>'
                ?>
            </div>
            <?php                
                if (!isset($_SESSION['paso']))
                    $_SESSION['paso']='no'; 
                if ($_SESSION['paso']==='si'){
                    $user=$_SESSION['user'];
                    include("module/user/view/read_user.php");
                }
                    ?>
            <br>
    		<table id="tablelist" class="display" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <td width=125><b><?php echo $lang_select['list_user']?></b></td>
                        <td width=125><b><?php echo $lang_select['list_DNI']?></b></td>
                        <td width=125><b><?php echo $lang_select['list_name']?></b></td>
                        <th width=350><b><?php echo $lang_select['list_action']?></b></td>
                    </tr>
                </thead>
                <tbody>
                <?php
                    if ($rdo->num_rows === 0){
                        echo '<tr>';
                        echo '<td align="center"  colspan="3">NO HAY NINGUN USUARIO</td>';
                        echo '</tr>';
                    }else{
                        foreach ($rdo as $row) {
                            ?>
                            <tr id="list">
                                <td width=125>
                                    <?php
                            	   	echo $row['usuario'];
                                    ?>
                                </td>
                                <td width=125>
                                    <?php
                            	   	echo $row['DNI'];
                                    ?>
                                </td>
                                <td width=125>
                                    <?php
                            	   	echo $row['nombre'];
                                    ?>
                                </td>
                                <td width=350>
                                    <?php
                                    //href="index.php?page=controller_user&op=read&id='.$row['usuario'].'"
                                        echo '<a class="Button_blue"  id="'.$row['usuario'].'">Read</a>';
                                	   	echo '&nbsp;';
                                	   	echo '<a class="Button_green" href="index.php?page=controller_user&op=update&id='.$row['usuario'].'">Update</a>';
                                	   	echo '&nbsp;';
                                	   	echo '<a class="Button_red" href="index.php?page=controller_user&op=delete&id='.$row['usuario'].'">Delete</a>';
                                    ?>
                                </td>
                            </tr>
                </tbody>
                            <?php
                        }
                    }
                ?>
            </table>
    	</div>
    </div>
</div>
</html>

<section id="user_modal">
    <div id="details_user" style="display:none;">
        <div id="details">
            <div id="container">
                Usuario: <div id="user1"></div></br>
                Nombre: <div id="name"></div></br>
                Apellidos: <div id="ape"></div></br>
                E-mail: <div id="email"></div></br>
                Contraseña: <div id="pass"></div></br>
                Fecha nacimiento: <div id="fnac"></div></br>
                Domicilio: <div id="domi"></div></br>
                Wallet: <div id="wallet"></div></br>
                Fecha registro: <div id="reg"></div></br>
                DNI: <div id="DNI"></div></br>
                Sexo: <div id="sex"></div></br>
            </div>
        </div>
    </div>
</section>