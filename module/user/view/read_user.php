<html>
<head>
    <link rel="stylesheet" type="text/css" href="view/css/style.css">
</head>
<div id="modal_read" class="modal">
    <div class="modal-content">
        <span class="close">&times;</span>
        <h1>Informacion del Usuario</h1>
        <p>
        <table border='2'>
            <tr>
                <td>Usuario: </td>
                <td>
                    <?php
                        echo $user['usuario'];
                    ?>
                </td>
            </tr>
        
            <tr>
                <td>Nombre: </td>
                <td>
                    <?php
                        echo $user['nombre'];
                    ?>
                </td>
            </tr>

            <tr>
                <td>Apellidos: </td>
                <td>
                    <?php
                        echo $user['apellidos'];
                    ?>
                </td>
            </tr>

            <tr>
                <td>Email: </td>
                <td>
                    <?php
                        echo $user['email'];
                    ?>
                </td>
            </tr>

            <tr>
                <td>Contraseña: </td>
                <td>
                    <?php
                        echo $user['pass'];
                    ?>
                </td>
            </tr>
            
            <tr>
                <td>Fecha de nacimiento: </td>
                <td>
                    <?php
                        echo $user['date_bithday'];
                    ?>
                </td>
            </tr>

            <tr>
                <td>Domicilio: </td>
                <td>
                    <?php
                        echo $user['domicilio'];
                    ?>
                </td>
            </tr>

            <tr>
                <td>Wallet: </td>
                <td>
                    <?php
                        echo $user['wallet'];
                    ?>
                </td>
            </tr>

            <tr>
                <td>Fecha y hora registro: </td>
                <td>
                    <?php
                        echo $user['fecha'];
                        echo '<br>';
                        echo $user['hora'];
                    ?>
                </td>
            </tr>

            <tr>
                <td>DNI: </td>
                <td>
                    <?php
                        echo $user['DNI'];
                    ?>
                </td>
            </tr>
            
            <tr>
                <td>Sexo: </td>
                <td>
                    <?php
                        echo $user['sexo'];
                    ?>
                </td>
            </tr>
            
            <tr>
                <td>Edad: </td>
                <td>
                    <?php
                        $fecha = $user['date_bithday'];
                        $troceada = explode('-', $fecha);
                        $date = date("Y:m:d");
                        $date_troceada = explode(':', $date);
                     /* echo $troceada[0];
                        echo '<br>';
                        echo $date_troceada[0];
                     */ print($date_troceada[0]-$troceada[0]);

                    ?>
                </td>
                
            </tr>

            <tr>
                <td>Aficiones: </td>
                <?php
                if ($user['aficiones']=="si"){
                    ?>
                <td>
                    <?php
                    if ($user['Bitcoin']=="si"){
                    ?>
                        Bitcoin
                    <?php
                    }
                    if ($user['IOTA']=="si"){
                    ?>
                        IOTA
                    <?php
                    }
                    if ($user['Litecoin']=="si") {
                    ?>
                        Litecoin
                    <?php
                    }
                    if ($user['Ethereum']=="si"){
                        ?>
                        Ethereum
                    <?php
                    }
                    ?>
                </td>
                <?php
                }else{
                    ?>
                <td>No hay aficiones guardadas</td>
                <?php
                }
                ?>
            </tr>
            
        </table>
        </p>
        <p><a id="volver" href="index.php?page=controller_user&op=list">Volver</a></p>
    </div>
    <?php
    $_SESSION['paso']='no';
     ?>
</div>
</html>
<script src="http://127.0.0.1/programacio/8_MVC_CRUD/module/user/model/read_modal.js"></script>