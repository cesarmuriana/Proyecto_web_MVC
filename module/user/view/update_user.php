<div id="contenido">
    <form autocomplete="on" method="post" name="update_user" id="update_user">
        <h1>Modificar usuario</h1>
        <img src="view/img/update.png" id="updatepng">
        <br>
        <div id="contenido_update">
            <table id="update_user">
                <tr>
                    <td>Usuario: </td>
                    <td><input type="text" id="usuario" name="usuario" placeholder="usuario" readonly value="<?php echo $user['usuario'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_usuario" class="e">
                                <?php
                                if (isset($error['usuario']))
                                    print_r($error['usuario']);

                                if (isset($existe_user))
                                    print_r("Este nombre de usuario no esta disponible");
                                ?>

                        </span>
                    </font></font></td>
                </tr>
            
                <tr>
                    <td>Nombre: </td>
                    <td><input type="text" id="nombre" name="nombre" placeholder="nombre" value="<?php echo $user['nombre'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_nombre" class="e">
                            <?php
                              if (isset($error['nombre']))
                                print_r($error['nombre']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Apellidos: </td>
                    <td><input type="text" id="apellidos" name="apellidos" placeholder="apellidos" value="<?php echo $user['apellidos'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_apellidos" class="e">
                            <?php
                                if (isset($error['apellidos']))
                                    print_r($error['apellidos']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Email: </td>
                    <td><input type="text" id="email" name="email" placeholder="email" value="<?php echo $user['email'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_email" class="e">
                            <?php
                                if (isset($error['email']))
                                    print_r($error['email']);
                                if (isset($existe_email))
                                    print_r("Este email ya ha sido asociado a otro usuario");
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Contraseña: </td>
                    <td><input type="password" id="pass" name="pass" placeholder="contraseña" value="<?php echo $user['pass'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_password" class="e">
                            <?php
                                if (isset($error['password']))
                                    print_r($error['password']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Repite Contraseña: </td>
                    <td><input type="password" id="pass2" name="pass2" placeholder="Repite contraseña" value="<?php echo $user['pass'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_password2" class="e">
                            <?php
                                if (isset($error['password']))
                                    print_r($error['password']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>
                
                <tr>
                    <td>Fecha de nacimiento: </td>
                    <td><input id="fecha" type="text" name="fecha_nacimiento" placeholder="fecha de nacimiento" readonly value="<?php echo $user['date_bithday'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_fecha_nacimiento" class="e">
                            <?php
                                if (isset($error['date_birthday']))
                                    print_r($error['date_birthday']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Domicilio: </td>
                    <td><input type="text" id="domicilio" name="domicilio" placeholder="domicilio" value="<?php echo $user['domicilio'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_domicilio" class="e">
                            <?php
                                if (isset($error['domicilio']))
                                    print_r($error['domicilio']);                        
                            ?>
                       </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>Wallet: </td>
                    <td><input type="text" id="wallet" name="wallet" placeholder="wallet" value="<?php echo $user['wallet'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_wallet" class="e">
                            <?php
                                if (isset($error['wallet']))
                                    print_r($error['wallet']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>DNI: </td>
                    <td><input type="text" id= "DNI" name="DNI" placeholder="DNI" readonly value="<?php echo $user['DNI'] ?>"/></td>
                    <td><font color="red">
                        <span id="e_DNI" class="e">
                            <?php
                                if (isset($error['DNI']))
                                    print_r($error['DNI']);
                                if (isset($existe_dni))
                                    print_r("Este DNI ya ha sido asociado a un usuario");
                            ?>
                        </span>
                    </font></font></td>
                </tr>
                
                <tr>
                    <td>Sexo: </td>
                        <?php
                        if ($user['sexo']==="Hombre") {
                        ?>
                            <td><input type="radio" id="sexo" name="sexo" placeholder="sexo" value="Hombre" checked/>Hombre
                                <input type="radio" id="sexo" name="sexo" placeholder="sexo" value="Mujer"/>Mujer</td>
                            <td>
                                <font color="red">
                        <?php
                        }else{
                        ?>
                            <td><input type="radio" id="sexo" name="sexo" placeholder="sexo" value="Hombre"/>Hombre
                                <input type="radio" id="sexo" name="sexo" placeholder="sexo" value="Mujer" checked/>Mujer</td>
                            <td>
                                <font color="red">
                        <?php
                        }
                        ?>
                        <span id="e_sexo" class="e">
                            <?php
                                if (isset($error['sexo']))
                                    print_r($error['sexo']);
                            ?>
                        </span>
                    </font></font></td>
                </tr>
                    
                </tr>

                <tr>
                    <td>Interesado en: </td>
                    <td>
                    <?php
                    if ($user['Bitcoin']=="Si"){
                        ?>
                        <input type="checkbox" id="Bitcoin" name="Bitcoin" value="Si" checked/>Bitcoin
                    <?php
                    }else{
                        ?>
                        <input type="checkbox" id="Bitcoin" name="Bitcoin" value="Si"/>Bitcoin
                        <?php
                    }
                    if ($user['IOTA']=="Si"){
                    ?>
                        <input type="checkbox" id= "IOTA" name="IOTA" value="Si" checked/>IOTA
                    <?php
                    }else{
                        ?>
                        <input type="checkbox" id= "IOTA" name="IOTA" value="Si"/>IOTA
                        <?php
                    }
                    if ($user['Litecoin']=="Si") {
                    ?>
                        <input type="checkbox" id= "Litecoin" name="Litecoin" value="Si" checked/>Litecoin
                    <?php
                    }else{
                        ?>
                        <input type="checkbox" id= "Litecoin" name="Litecoin" value="Si"/>Litecoin
                        <?php
                    }
                    if ($user['Ethereum']=="Si"){
                        ?>
                        <input type="checkbox" id= "Ethereum" name="Ethereum" value="Si" checked/>Ethereum
                    <?php
                    }else{
                        ?>
                        <input type="checkbox" id= "Ethereum" name="Ethereum" value="Si"/>Ethereum
                        <?php
                    }
                    ?>
                    </td>
                    <td><font color="red">
                        <span id="e_aficion" class="e">
                            <?php
                    //            echo "No hay ninguna aficion";
                            ?>
                        </span>
                    </font></font></td>
                </tr>

                <tr>
                    <td>
                        <input type="hidden" value="update" name="update" id="update"/>
                        <input type="button" value="Aceptar" name="update" id="aceptar" onclick="validate_user()"
                        /></td>
                    <td align="right"><a id="volver" href="index.php?page=controller_user&op=list">Volver</a></td>
                </tr>
            </table>
        </div>
    </form>
</div>