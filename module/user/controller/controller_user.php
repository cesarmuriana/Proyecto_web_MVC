<?php
$path = $_SERVER['DOCUMENT_ROOT'] . '/programacio/8_MVC_CRUD/';
    include ($path."module/user/model/DAOUser.php");
    switch($_GET['op']){
        case 'list';
            try{
                $daouser = new DAOUser();
            	$rdo = $daouser->select_all_user();
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
    			$callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
             
                include("module/user/view/list_user.php");
    		}
            break;
            
        case 'create';
            include("module/user/model/validate.php");
           
            $check = true;
            
            if (isset($_POST['create'])){
                $check=validate_user();
                $error=$check['error'];
                if (empty($error)){
                    $_SESSION['usuario']=$_POST;
                    try{
                        $daouser = new DAOUser();
                        $existe_user = $daouser->find_user($_POST['usuario']);
                        $existe_dni = $daouser->find_dni($_POST['DNI']);
                        $existe_email = $daouser->find_email($_POST['email']);
                        if ((!isset($existe_user)) and (!isset($existe_dni)) and (!isset($existe_email)))
    		              $rdo = $daouser->insert_user($_POST);
                    }catch (Exception $e){
                        $callback = 'index.php?page=503';
        			    die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    
                    if(isset($existe_user)){
                        echo '<script language="javascript">alert("Este usuario ya ha sido asociado")</script>';
                    }elseif(isset($existe_dni)){
                        echo '<script language="javascript">alert("Este DNI esta asociado a otro usuario")</script>';
                    }elseif(isset($existe_email)){
                        echo '<script language="javascript">alert("Este email esta asociado a otro usuario")</script>';
                        }else{
        		            if($rdo){
                       			echo '<script language="javascript">alert("Registrado en la base de datos correctamente")</script>';
                    			$callback = 'index.php?page=controller_user&op=list';
                			    die('<script>window.location.href="'.$callback .'";</script>');
                    		}else{
                    			$callback = 'index.php?page=503';
            			        die('<script>window.location.href="'.$callback .'";</script>');
                    		}
                        }
                    
                }
            }
            include("module/user/view/create_user.php");
            break;
            
        case 'update';
            include("module/user/model/validate.php");
           
            $check = true;
            
            if (isset($_POST['update'])){
                $check=validate_user();
                $error=$check['error'];
                if (empty($error)){
                    $_SESSION['usuario']=$_POST;
                    try{
                        $daouser = new DAOUser();
                        $rdo = $daouser->update_user($_POST);
                    }catch (Exception $e){
                        
                        $callback = 'index.php?page=503';
        			    die('<script>window.location.href="'.$callback .'";</script>');
                    }
                    

                    if($rdo){
                        echo '<script language="javascript">alert("Actualizado en la base de datos correctamente")</script>';
                        $callback = 'index.php?page=controller_user&op=list';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }else{
                        $callback = 'index.php?page=503';
                        die('<script>window.location.href="'.$callback .'";</script>');
                    }
                }
            }
            
            try{
                $daouser = new DAOUser();
            	$rdo = $daouser->select_user($_GET['id']);
            	$user=get_object_vars($rdo);
            }catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            
            if(!$rdo){
    			$callback = 'index.php?page=503';
    			die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
        	    include("module/user/view/update_user.php");
    		}
            break;
            
        case 'read';
            try{
                $daouser = new DAOUser();
            	$rdo = $daouser->select_user($_GET['id']);
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            if(!$rdo){
                echo json_encode("error");
                exit;
            }else{
                $user=get_object_vars($rdo);
                echo json_encode($user);
                exit;
            }




            /*catch (Exception $e){
                $callback = 'index.php?page=503';
			    die('<script>window.location.href="'.$callback .'";</script>');
            }
            /*if(!$rdo){
    			$callback = 'index.php?page=503';
    			die('<script>window.location.href="'.$callback .'";</script>');
    		}else{
            $_SESSION['user']=$user;
            $_SESSION['paso']='si';

            die('<script>window.location.href="index.php?page=controller_user&op=list";</script>');
    		}*/
            break;
            
        case 'delete';
            if (isset($_POST['delete'])){
                try{
                    $daouser = new DAOUser();
                	$rdo = $daouser->delete_user($_GET['id']);
                }catch (Exception $e){
                    $callback = 'index.php?page=503';
    			    die('<script>window.location.href="'.$callback .'";</script>');
                }
            	
           	if($rdo){
        			echo '<script language="javascript">alert("Borrado en la base de datos correctamente")</script>';
        			$callback = 'index.php?page=controller_user&op=list';
    			    die('<script>window.location.href="'.$callback .'";</script>');
        		}else{
        			$callback = 'index.php?page=503';
			        die('<script>window.location.href="'.$callback .'";</script>');
        		}
            }
            
              include("module/user/view/delete_user.php");
              break;
        case 'dumies';
                $daouser = new DAOUser();
                $rdo = $daouser->dumies();
                if($rdo==='null'){
                        echo '<script language="javascript">alert("Los datos ya estan establecidos")</script>';
                    }else{
                        if($rdo){
                            echo '<script language="javascript">alert("Dumies insertados correctamente")</script>';
                            $callback = 'index.php?page=controller_user&op=list';
                            die('<script>window.location.href="'.$callback .'";</script>');
                        }
                    }
                $callback = 'index.php?page=controller_user&op=list';
                die('<script>window.location.href="'.$callback .'";</script>');
            break;
        default;
            include("view/inc/error404.php");
            break;
    }