var cart = [];
        $(function () {
            if (localStorage.cart){
                cart = JSON.parse(localStorage.cart);
                showCart();
            }
        });

        function getinfo(id){
        	var orgid = id.split(" ");
	        var id = orgid[0];

                getpricecrypto(id).then(function(usd) {
                    getpriceur().then(function(eur) {
                        var prieur= usd*eur;
                        var data={
                        priceur: prieur,
                        id: id
                        };
                        addToCart(data);
                        window.location.replace("index.php?page=controller_carrito&op=carrito");
                    })
                })
        }
        function addToCart(data) {
                //deu de ser un array
                var price = data.priceur;
                var name = data.id;
                var qty = "1";

                // update qty if product is already present
                for (var i in cart) {
                    if(cart[i].Product == name){
                        cart[i].Qty = qty;
                        showCart();
                        saveCart();
                        return;
                    }
                }
                // create JavaScript Object
                var item = { Product: name,  Price: price, Qty: qty }; 
                cart.push(item);
                saveCart();
                showCart();
        }

        function deleteItem(index){
            cart.splice(index,1); // delete item at index
            showCart();
            saveCart();
        }

        function saveCart() {
            if ( window.localStorage){
                localStorage.cart = JSON.stringify(cart);
            }
        }

        function calculatotelcarrito(){
                var precios=document.getElementById("compras");
                var todo = [precios];
                var datos = todo[0].childNodes;
                var sumatot=0;
                var i = "true";
                var cont=0;
                while ( i === "true") {
                    try {
                        var precio = [datos[cont]];//datos[*] es lo que tinc que cambiar per recorrer tots els productes
                        var precio2 = precio[0].innerText.split(" ");
                        sumatot=(parseFloat(sumatot))+(parseFloat(precio2[7]));
                        cont++;
                    }catch(err){
                        i="false";
                    }
                }
                $("#precio_total").html("El precio total es: "+sumatot.toFixed(2)+"€");
            };

        function showCart() {
            if (cart.length == 0) {
                $("#compras").css("visibility", "hidden");
                return;
            }

            $("#compras").css("visibility", "visible");
            $("#compras").empty();
            for (var i in cart) {
                var item = cart[i];
                var row = "<div id='carro_entero' onchange='sumaprecio(this),calculatotelcarrito()' class='"+item.Product+"'><div id='nom_prod'>" + item.Product + 
                "</div><div id='carro'><input type='number' min='1' id='carrito_cantidad' class='cantidad"+item.Product+"' value='"+item.Qty+"'>"+
                "</div><div id='precio_item'>" + item.Price.toFixed(2)+"<a>€</a>" + "</div><div id='carro'>" + 
                "</div><div id='precio_productos' class='precio_productos"+item.Product+"'>"+item.Price.toFixed(2)+'€'+"</div><div id='carro'><button id='eliminar' onclick='deleteItem("
                + i +"),calculatotelcarrito()'>X</button></div></div>";
                $("#compras").append(row);
            }
        }
        function getpriceur() {
            const promise = new Promise(function(resolve, reject) {
                $.ajax({
                type: "GET",
                url: "https://api.fixer.io/latest?base=USD",                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              
                success: function(data){
                    //console.log(data.rates.EUR);
                    var euro=data.rates.EUR;
                    resolve(euro);
                }
                });
            });
           return promise
        };
        function getpricecrypto(moneda){
          const promise = new Promise(function(resolve, reject) {
                $.ajax({
                type: "GET",
                dataType: "json",
                url: "https://api.coinmarketcap.com/v1/ticker/"+moneda,
                success: function(data){
                    console.log(data);
                    var priceusd=data[0].price_usd;
                    resolve(priceusd);
                }
                });
            });
            return promise;
        }



        function sumaprecio(classe) {
                var datos =[classe];


                var idcant = (datos[0].className);
                var cant = document.getElementsByClassName("cantidad"+idcant);
                var cantidad = cant.carrito_cantidad.value;
                var precio = datos[0].outerText.split(" ");
                var preciou = precio[4];

                var preciousolo = preciou.replace(/€/,'');
                var partireur = preciousolo.split(".");

                var mult = (parseFloat(cantidad)) * (parseFloat(preciousolo));


                $(".precio_productos"+idcant).html(mult.toFixed(2)+"€");
                //guardar cantidad especificada en carrito
                for (var i in cart) {
                    if(cart[i].Product == idcant){
                        cart[i].Qty = cantidad;
                        saveCart();
                        return;
                    }
                }
        };
        $(document).ready(function() {
            calculatotelcarrito();
            $('#compra').click(function(){

                var precios=document.getElementById("compras");
                var todo = [precios];
                var datos = todo[0].childNodes;
                var sumatot=0;
                var i = "true";
                var cont=0;
                while ( i === "true") {
                    try {
                        var precio = [datos[cont]];//datos[*] es lo que tinc que cambiar per recorrer tots els productes
                        var precio2 = precio[0].innerText.split(" ");
                        cont++;
                    }catch(err){
                        i="false";
                    }
                }
                                
                try {
                    var menu=[document.getElementsByClassName("nombre_usuario")];
                    var menu1 = [menu];
                    var menu =[menu1[0][0][0].innerText.split(" ")];
                    var nombre=menu[0][2];
                    var valido="true";
                }catch(err){
                    window.location.replace("index.php?page=controller_login&op=compra");
                    
                }

                if ( valido === "true"){
                    $.ajax({
                    type: "POST",
                    url: "module/carrito/controller/controller_carrito.php?op=checkout",
                    data:{cart,cont,nombre},
                    success: function(data){
                            var borrar=-parseInt(cont);
                            alert(data);
                            while ( borrar !== cont ) {
                                deleteItem(borrar);
                                borrar++;
                            }
                            
                        }
                    });
                    var id = document.getElementById('compras');
                    id.style.display = 'none';
                }
            });
        });