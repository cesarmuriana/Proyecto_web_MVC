-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-06-2016 a las 08:19:43
-- Versión del servidor: 5.5.49-0ubuntu0.14.04.1
-- Versión de PHP: 5.5.9-1ubuntu4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: 'DB'
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla 'usuario'
--

CREATE TABLE IF NOT EXISTS 'usuario' (
  'usuario' varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  'nombre' varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  'apellidos' varchar(50) COLLATE utf8_spanish_ci NOT NULL,
  'email' varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  'password' varchar(20) COLLATE utf8_spanish_ci NOT NULL,
  'date_birthday' varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  'domicilio' varchar(100) NOT NULL,
  'wallet' varchar(37) COLLATE utf8_spanish_ci NOT NULL,
  'hora' varchar(10) COLLATE utf8_spanish_ci NOT NULL,
  'fecha' varchar(200) COLLATE utf8_spanish_ci NOT NULL,
  'DNI' varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  'sexo' varchar(9) COLLATE utf8_spanish_ci NOT NULL,
  PRIMARY KEY ('usuario')
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_spanish_ci;

--
-- Volcado de datos para la tabla 'usuario'
--
INSERT INTO `dbitcoin`.`usuario` (`usuario`, `nombre`, `apellidos`, `email`, `pass`, `date_bithday`, `domicilio`, `wallet`, `hora`, `fecha`, `DNI`, `sexo`) 
VALUES ('User-123', 'angel', 'ancoca', 'angelcoca@gmail.com', '123456LALA', '1993-19-07', 'C/ Copérnico nº6 (Pol. Ind. De La Grela)', '1HdfV7iKMRbpvKZu8QmJtuXBzFMn3UiHjy', '13:36', '2017/25/10', '48606014w', 'Hombre');



INSERT INTO `dbitcoin`.`usuario` (`usuario`, `nombre`, `apellidos`, `email`, `pass`, `date_bithday`, `domicilio`, `wallet`, `hora`, `fecha`, `DNI`, `sexo`, `aficiones`, `Bitcoin`, `IOTA`, `Litecoin`, `Ethereum`)
VALUES ('User-123', 'angel', 'ancoca', 'angelcoca@gmail.com', '123456LALA', '1993-19-07', 'C/ Copérnico nº6 (Pol. Ind. De La Grela) Edificio BCA 28. Oficina: -1.1', '1HdfV7iKMRbpvKZu8QmJtuXBzFMn3UiHjy', now(), now(), '95646231k', 'Hombre', 'si', 'si', 'no', 'si', 'si'),
('User-124', 'david', 'daurgil', 'davidgil@gmail.com', '123456LOLO', '1990-06-06', 'Avenida Pintor Sorolla 125 4ºG', '3L1WohGdpS3ARz8W5ofVgZBCV94n9fQ2wj', now(), now(), '78949524a', 'Hombre', 'si', 'no', 'si', 'no', 'si'),
('User-125', 'usuarionom', 'usuarioapell', 'usuarionomapell@gmail.com', '123456SUP', '1980-05-16', 'Calle San Romualdo 13', '3GyqjsSqJ5bt1n7mRihm1kAGRsgJBoqp9A', now(), now(), '48601445l', 'Hombre', 'no', 'no', 'no', 'si', 'no');
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

INSERT INTO `dbitcoin`.`crypto` (`cryptos `)
VALUES
('Bitcoin'),
('Ethereum'),
('Ripple'),
('Bitcoin Cash'),
('Cardano'),
('Litecoin'),
('Nem'),
('Neo'),
('Stellar'),
('Iota'),
('Dash'),
('Eos'),
('Tron'),
('Monero'),
('Bitcoin Gold'),
('Icon'),
('Ethereum Classic'),
('Qtum'),
('Lisk'),
('RaiBlocks'),
('Omisego'),
('Siacoin'),
('Verge')