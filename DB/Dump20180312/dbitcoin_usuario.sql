-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: dbitcoin
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `usuario`
--

DROP TABLE IF EXISTS `usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(30) DEFAULT NULL,
  `nombre` varchar(30) DEFAULT NULL,
  `apellidos` varchar(30) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `pass` varchar(100) DEFAULT NULL,
  `date_bithday` varchar(30) DEFAULT NULL,
  `domicilio` varchar(100) DEFAULT NULL,
  `wallet` varchar(50) DEFAULT NULL,
  `hora` time DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `DNI` varchar(9) DEFAULT NULL,
  `sexo` varchar(6) DEFAULT NULL,
  `aficiones` varchar(100) DEFAULT NULL,
  `Bitcoin` varchar(2) DEFAULT NULL,
  `IOTA` varchar(2) DEFAULT NULL,
  `Litecoin` varchar(2) DEFAULT NULL,
  `Ethereum` varchar(2) DEFAULT NULL,
  `compras` char(255) DEFAULT NULL,
  `bitcoin_compra` char(255) DEFAULT NULL,
  `iota_compra` char(255) DEFAULT NULL,
  `litecoin_compra` char(255) DEFAULT NULL,
  `ethereum_compra` char(255) DEFAULT NULL,
  `monedas` char(60) DEFAULT NULL,
  `descripcion` char(200) DEFAULT NULL,
  `type` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=103 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuario`
--

LOCK TABLES `usuario` WRITE;
/*!40000 ALTER TABLE `usuario` DISABLE KEYS */;
INSERT INTO `usuario` VALUES (1,'admin',NULL,NULL,NULL,'admin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'',NULL,NULL,NULL,NULL,NULL,'admin'),(15,'El_cesar06','cesar','muriana goya','cesarmuriana1@gmail.com','123456','1999-01-10','Calle Gaspar Blai Arbuixec Nº12','1H6TvSk9USM5gwVtWddKLvopoaK5pAG3WD','16:00:00','2003-09-23','48606013k','Hombre','si','si','si','no','no','si','3','30','6','0',NULL,NULL,'user'),(60,'elcesar','Cesar2','apee','cesarmuriana','Unapass','1999-11-02','una calle','isabdusabuasdybasuydbusado','20:12:35','2018-01-07','48606013p','Hombre','si','si','no','no','no','si','3','0','0','0',NULL,NULL,'user'),(65,'cesar0606','cesar','Goya','cesarmuriana@gmail.com','elcesar','1999-01-10','asdsad','1GbdmKoJBQq56UVwy2cUKWr','20:20:39','2018-01-07','48606013O','Mujer','Si','No','Si','No','Si','si','2','5','1','3',NULL,NULL,'user'),(88,'User-124','david','daurgil','davidgil@gmail.com','123456LOLO','1990-06-06','Avenida Pintor Sorolla 125 4ÂºG','3L1WohGdpS3ARz8W5ofVgZBCV94n9fQ2wj','13:03:33','2018-01-30','78949524a','Hombre','si','no','si','no','si','si','3','6','4','3',NULL,NULL,'user'),(95,'ces3','cesar',NULL,'ces@gmail.com','cesar',NULL,NULL,NULL,'19:19:00','2018-02-01',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'user'),(96,'user','user','user','user@user.com','cesar0606','2018-02-14','sadjsanisajn','65s1a65s16asd15','16:41:01','2018-02-05','48606013V','Hombre','Si','Si','No','No','Si','si','1','2','55','1',NULL,NULL,'user'),(97,'asada','asada','asada','asada@asada.as','asadada','2018-02-15','adasad','adasad','00:03:47','2018-02-06','48606013M','Hombre','Si','No','Si','No','No','no',NULL,NULL,NULL,NULL,NULL,NULL,'user'),(98,'sahjudsuad','iujbdassaudb','iubadsausdb','asodin@ausdhib.com','123456','1917-02-07','sadsadsa','asdasdasd','00:39:07','2018-02-06','48606019K','Hombre','Si','Si','No','No','No','no',NULL,NULL,NULL,NULL,NULL,NULL,'user'),(99,'cesar',NULL,NULL,NULL,'cesar',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'user'),(100,'User-123','angel','ancoca','angelcoca@gmail.com','123456LALA','1993-19-07','C/ CopÃ©rnico nÂº6 (Pol. Ind. De La Grela) Edificio BCA 28. Oficina: -1.1','1HdfV7iKMRbpvKZu8QmJtuXBzFMn3UiHjy','19:12:49','2018-03-08','95646231k','Hombre','si','si','no','si','si',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(101,'User-125','usuarionom','usuarioapell','usuarionomapell@gmail.com','123456SUP','1980-05-16','Calle San Romualdo 13','3GyqjsSqJ5bt1n7mRihm1kAGRsgJBoqp9A','19:12:49','2018-03-08','48601445l','Hombre','no','no','no','no','no',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(102,'elcesar0606060606','cesadsda','jhbsad','sajniasdsad@gmail.com','123456','1999-03-10','asdsa','jsadhbsdadsjhbasbsad','19:18:26','2018-03-08','48606014H','Hombre','Si','Si','Si','No','No',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'user');
/*!40000 ALTER TABLE `usuario` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-12 18:45:18
