-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: dbitcoin
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `like`
--

DROP TABLE IF EXISTS `like`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `like` (
  `usuario` varchar(50) DEFAULT NULL,
  `preferencias` varchar(50) DEFAULT NULL,
  `bitcoin` varchar(50) DEFAULT NULL,
  `ethereum` varchar(50) DEFAULT NULL,
  `ripple` varchar(50) DEFAULT NULL,
  `bitcoin_cash` varchar(50) DEFAULT NULL,
  `cardano` varchar(50) DEFAULT NULL,
  `litecoin` varchar(50) DEFAULT NULL,
  `nem` varchar(50) DEFAULT NULL,
  `neo` varchar(50) DEFAULT NULL,
  `stellar` varchar(50) DEFAULT NULL,
  `iota` varchar(50) DEFAULT NULL,
  `dash` varchar(50) DEFAULT NULL,
  `eos` varchar(50) DEFAULT NULL,
  `tron` varchar(50) DEFAULT NULL,
  `monero` varchar(50) DEFAULT NULL,
  `bitcoin_gold` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `ethereum_classic` varchar(50) DEFAULT NULL,
  `qtum` varchar(50) DEFAULT NULL,
  `lisk` varchar(50) DEFAULT NULL,
  `nano` varchar(50) DEFAULT NULL,
  `omisego` varchar(50) DEFAULT NULL,
  `siacoin` varchar(50) DEFAULT NULL,
  `verge` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `like`
--

LOCK TABLES `like` WRITE;
/*!40000 ALTER TABLE `like` DISABLE KEYS */;
INSERT INTO `like` VALUES ('cesar',NULL,'Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si'),('cesar',NULL,'Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si'),('cesar',NULL,'Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si','Si');
/*!40000 ALTER TABLE `like` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-12 18:45:18
